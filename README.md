# how to diff odt/doc documents #

First, install odt2txt, and configure git to allow it to run it, by adding this to ~/.gitconfig:

[diff "odf"]  
textconv=odt2txt

Now, for each project, you just need to ask git to use this driver in .gitattributes or $GIT_DIR/info/attributes, like this:

\*.ods diff=odf  
\*.odt diff=odf  
\*.odp diff=odf